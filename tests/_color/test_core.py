import colorsys
import fractions

import hypothesis.strategies
import pytest

from wclr._color.core import Color


class TestColor:

    @hypothesis.given(
        hypothesis.strategies.fractions(),
        hypothesis.strategies.fractions(),
        hypothesis.strategies.fractions(),
        hypothesis.strategies.fractions(min_value=0, max_value=1))
    def test_to_srgb_tuple(self, r, g, b, alpha):
        color = Color(r, g, b, alpha=alpha)
        actual = color.to_srgb_tuple()

        desired = (r, g, b, alpha)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255))
    def test_from_str_six_digit_hex(self, r, g, b):
        actual = Color.from_str(f"#{r:02x}{g:02x}{b:02x}")
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=15),
        hypothesis.strategies.integers(min_value=0, max_value=15),
        hypothesis.strategies.integers(min_value=0, max_value=15))
    def test_from_str_three_digit_hex(self, r, g, b):
        actual = Color.from_str(f"#{r:x}{g:x}{b:x}")
        desired = Color(
            fractions.Fraction(r, 15),
            fractions.Fraction(g, 15),
            fractions.Fraction(b, 15))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255))
    def test_to_hex_str_force_six_digit(self, r, g, b):
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255))
        actual = Color.from_str(desired.to_hex_str(force_six_digit=True))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=15),
        hypothesis.strategies.integers(min_value=0, max_value=15),
        hypothesis.strategies.integers(min_value=0, max_value=15))
    def test_to_hex_str_not_force_six_digit(self, r, g, b):
        desired = Color(
            fractions.Fraction(r, 15),
            fractions.Fraction(g, 15),
            fractions.Fraction(b, 15))
        actual = Color.from_str(desired.to_hex_str(force_six_digit=False))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255))
    def test_from_str_rgb_function_integer(self, r, g, b):
        actual = Color.from_str(f"rgb({r},{g},{b})")
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_from_str_rgb_function_percentage(self, r, g, b):
        actual = Color.from_str(f"rgb({r:%},{g:%},{b:%})")
        desired = Color(r, g, b)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_from_str_rgba_function_integer(self, r, g, b, alpha):
        actual = Color.from_str(f"rgba({r},{g},{b},{alpha:f})")
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255),
            alpha=alpha)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_from_str_rgba_function_percentage(self, r, g, b, alpha):
        actual = Color.from_str(f"rgba({r:%},{g:%},{b:%},{alpha:f})")
        desired = Color(r, g, b, alpha=alpha)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_to_rgb_function_str_integer_force_rgba(self, r, g, b, alpha):
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255),
            alpha=alpha)
        actual = Color.from_str(
            desired.to_rgb_function_str(percentage=False, force_rgba=True))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255),
        hypothesis.strategies.integers(min_value=0, max_value=255))
    def test_to_rgb_function_str_integer_not_force_rgba(self, r, g, b):
        desired = Color(
            fractions.Fraction(r, 255),
            fractions.Fraction(g, 255),
            fractions.Fraction(b, 255))
        actual = Color.from_str(
            desired.to_rgb_function_str(percentage=False, force_rgba=False))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_to_rgb_function_str_percentage_force_rgba(self, r, g, b, alpha):
        desired = Color(r, g, b, alpha=alpha)
        actual = Color.from_str(
            desired.to_rgb_function_str(percentage=True, force_rgba=True))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_to_rgb_function_str_percentage_not_force_rgba(self, r, g, b):
        desired = Color(r, g, b)
        actual = Color.from_str(
            desired.to_rgb_function_str(percentage=True, force_rgba=False))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1))
    def test_from_hsl(self, hue, saturation, lightness, alpha):
        color = Color.from_hsl(hue, saturation, lightness, alpha=alpha)
        actual = color.to_srgb_tuple()

        desired_rgb = colorsys.hls_to_rgb(hue, lightness, saturation)
        desired = desired_rgb + (alpha,)

        assert actual == pytest.approx(desired)

    @hypothesis.given(
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1),
        hypothesis.strategies.fractions(min_value=0, max_value=1))
    def test_to_hsl_tuple(self, r, g, b, alpha):
        color = Color(r, g, b, alpha=alpha)
        h, s, l, actual_alpha = color.to_hsl_tuple()
        actual_rgb = colorsys.hls_to_rgb(h, l, s)
        actual = actual_rgb + (actual_alpha,)

        desired = (r, g, b, alpha)

        assert actual == pytest.approx(desired)

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=360),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_from_str_hsl_function(self, hue, saturation, lightness):
        actual = Color.from_str(f"hsl({hue:f},{saturation:%},{lightness:%})")
        desired = Color.from_hsl(
            fractions.Fraction(hue) / 360, saturation, lightness)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=360),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_from_str_hsla_function(self, hue, saturation, lightness, alpha):
        actual = Color.from_str(
            f"hsla({hue:f},{saturation:%},{lightness:%},{alpha:f})")
        desired = Color.from_hsl(
            fractions.Fraction(hue) / 360, saturation, lightness, alpha=alpha)

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_to_hsl_function_str_force_hsla(
            self, hue, saturation, lightness, alpha):
        desired = Color.from_hsl(hue, saturation, lightness, alpha=alpha)
        actual = Color.from_str(desired.to_hsl_function_str(force_hsla=True))

        assert actual == desired

    @hypothesis.given(
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1),
        hypothesis.strategies.decimals(min_value=0, max_value=1))
    def test_to_hsl_function_str_not_force_hsla(
            self, hue, saturation, lightness):
        desired = Color.from_hsl(hue, saturation, lightness)
        actual = Color.from_str(desired.to_hsl_function_str(force_hsla=False))

        assert actual == desired
