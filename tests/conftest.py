import hypothesis.strategies
import pytest

from wclr._color.core import Color


@pytest.helpers.wclr.register
@hypothesis.strategies.composite
def colors(draw, min_alpha=0, max_alpha=1):
    r = draw(hypothesis.strategies.fractions(min_value=0, max_value=1))
    g = draw(hypothesis.strategies.fractions(min_value=0, max_value=1))
    b = draw(hypothesis.strategies.fractions(min_value=0, max_value=1))
    alpha = draw(
        hypothesis.strategies.fractions(
            min_value=min_alpha, max_value=max_alpha))

    return Color(r, g, b, alpha=alpha)
