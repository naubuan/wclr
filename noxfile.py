import pathlib
import shutil

import nox


SUPPORTED_PYTHONS = ["3.8", "3.9", "3.10", "3.11"]

PACKAGE_DIR = pathlib.Path("src") / "wclr"
DOCS_DIR = pathlib.Path("docs")
REQUIREMENTS_PATHS = {
    path.stem: path for path in pathlib.Path("requirements").glob("*.txt")}

REPOSITORIES = {
    "testpypi": "https://test.pypi.org/legacy/",
    "pypi": "https://upload.pypi.org/legacy/"}


nox.needs_version = ">= 2021.10.1"
nox.options.sessions = ["lint", "type", "test", "apidoc", "doctest", "doc"]


@nox.session
def lint(session):
    """Check the code style of the package."""
    session.install("-r", str(REQUIREMENTS_PATHS["lint"]))
    session.run("flake8", *session.posargs, str(PACKAGE_DIR))


@nox.session
def type(session):
    """Run static type checks."""
    session.install("-r", str(REQUIREMENTS_PATHS["type"]))
    session.run("mypy", *session.posargs)


@nox.session(python=SUPPORTED_PYTHONS)
def test(session):
    """Run tests."""
    session.install("-r", str(REQUIREMENTS_PATHS["test"]))
    session.run(
        "coverage", "run", "-m", "pytest", *session.posargs,
        success_codes=[0, 5])
    session.run("coverage", "report")


@nox.session
def apidoc(session):
    """Generate source files of the API documentation."""
    api_dir = DOCS_DIR / "contents" / "api"

    shutil.rmtree(api_dir / "generated", ignore_errors=True)

    session.install("-r", str(REQUIREMENTS_PATHS["doc"]))
    session.run(
        "sphinx-autogen", "-a", "-i", "-t", str(DOCS_DIR / "_templates"),
        str(api_dir / "index.rst"))


@nox.session(python=SUPPORTED_PYTHONS)
def doctest(session):
    """Test codes in the documentation."""
    session.install("-r", str(REQUIREMENTS_PATHS["doc"]))
    session.run(
        "sphinx-build", "-b", "doctest", "-W", str(DOCS_DIR),
        str(DOCS_DIR / "_build" / "doctest"))


@nox.session
def doc(session):
    """Build the documentation."""
    session.install("-r", str(REQUIREMENTS_PATHS["doc"]))
    session.run(
        "sphinx-build", "-b", "html", "-W", *session.posargs, str(DOCS_DIR),
        str(DOCS_DIR / "_build" / "html"))


@nox.session
@nox.parametrize("repository", list(REPOSITORIES))
def upload(session, repository):
    """Upload the distribution."""
    options = session.posargs + ["--repository-url", REPOSITORIES[repository]]

    session.install("-r", str(REQUIREMENTS_PATHS["upload"]))
    session.run("python", "-m", "build")
    session.run("twine", "check", "--strict", "dist/*")
    session.run("twine", "upload", *options, "dist/*")
