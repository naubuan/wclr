{{ fullname | escape | underline }}

.. currentmodule:: {{ module }}

.. testsetup::

   from {{ module }} import {{ objname }}

.. autofunction:: {{ objname }}
