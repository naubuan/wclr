{{ fullname | escape | underline }}

.. currentmodule:: {{ module }}

.. testsetup::

   from {{ module }} import {{ objname }}

.. autoclass:: {{ objname }}

   {%- block attributes %}
   {%- if attributes %}

   .. rubric:: {{ _("Attributes") }}

   .. autosummary::
      :toctree:
{% for item in attributes %}
      ~{{ name }}.{{ item }}
{%- endfor %}
   {%- endif %}
   {%- endblock %}

   {%- block methods %}
   {%- set methods = methods | reject("==", "__init__") | list %}
   {%- if methods %}

   .. rubric:: {{ _("Methods") }}

   .. autosummary::
      :toctree:
{% for item in methods %}
      ~{{ name }}.{{ item }}
{%- endfor %}
   {%- endif %}
   {%- endblock %}

