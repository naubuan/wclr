import pathlib


_package_dir = pathlib.Path(__file__).resolve().parents[1] / "src" / "wclr"

author = "naubuan"
copyright = f"2023, {author}"
exclude_patterns = ["_build"]
highlight_language = "python"
modindex_common_prefix = [f"{_package_dir.name}."]
project = "WClr"
templates_path = ["_templates"]
today_fmt = "%B %-d, %Y"

_ns = {}
with open(_package_dir / "_version.py", mode="r") as _f:
    exec(_f.read(), {}, _ns)
release = version = _ns["__version__"]

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_copybutton"]

# sphinx.ext.autodoc
autodoc_default_options = {"show-inheritance": True}
autodoc_typehints = "none"

# sphinx.ext.autosummary
autosummary_generate = False

# sphinx.ext.intersphinx
intersphinx_mapping = {"python": ("https://docs.python.org/3/", None)}

# sphinx.ext.napoleon
napoleon_preprocess_types = True
napoleon_type_aliases = {"fraction-like": ":term:`fraction-like`"}
napoleon_use_admonition_for_notes = True
napoleon_use_rtype = False

# sphinx_copybutton
copybutton_prompt_text = ">>> "
copybutton_exclude = ".lineos"

# HTML
html_copy_source = False
html_theme = "furo"
html_title = f"{project} documentation"
