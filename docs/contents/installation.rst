Installation
============

WClr requires Python version 3.8 or later. You can install WClr by, e.g.,
`pip <https://pip.pypa.io/>`_:

.. code-block:: shell

   python -m pip install wclr
