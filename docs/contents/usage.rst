Usage
=====

Color class
-----------

A color is represented by a :class:`wclr.Color` instance. You can construct it
from a `CSS color <https://www.w3.org/TR/css-color-3/>`_:

   >>> import wclr
   >>> wclr.Color.from_str("#ffffff")
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color.from_str("rgb(255,255,255)")
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color.from_str("rgba(255,255,255,0.5)")
   <Color: (1, 1, 1) in sRGB, alpha=1/2>
   >>> wclr.Color.from_str("hsl(0,0%,100%)")
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color.from_str("hsla(0,0%,100%,0.5)")
   <Color: (1, 1, 1) in sRGB, alpha=1/2>
   >>> wclr.Color.from_str("white")
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color.from_str("transparent")
   <Color: (0, 0, 0) in sRGB, alpha=0>

Alternatively, you can construct it from sRGB components and HSL components:

   >>> wclr.Color(1, 1, 1)
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color(1, 1, 1, alpha=0)
   <Color: (1, 1, 1) in sRGB, alpha=0>

   >>> wclr.Color.from_hsl(0, 0, 1)
   <Color: (1, 1, 1) in sRGB, alpha=1>
   >>> wclr.Color.from_hsl(0, 0, 1, alpha=0)
   <Color: (1, 1, 1) in sRGB, alpha=0>

A :class:`wclr.Color` instance can be converted into a CSS color by the
:meth:`~wclr.Color.to_hex_str`, :meth:`~wclr.Color.to_rgb_function_str`, and
:meth:`~wclr.Color.to_hsl_function_str` methods. It can also be converted into
sRGB and HSL components by the :meth:`~wclr.Color.to_srgb_tuple` and
:meth:`~wclr.Color.to_hsl_tuple` methods.

.. note::

   The :class:`wclr.Color` class uses the :class:`fractions.Fraction` class
   internally, and therefore, it is free from :doc:`limitations of
   floating-point numbers <python:tutorial/floatingpoint>` except for the
   conversion of a repeating decimal into a :class:`str` instance. If you
   already have a :term:`fraction-like` instance, e.g., a :class:`str` instance
   of ``'0.1'``, then it is recommended that you pass it to the
   :class:`wclr.Color` class as it is rather than as a :class:`float` instance.

Other features
--------------

* The CSS keyword colors can be obtained by the :func:`wclr.keyword_strs`
  function.
* You can check if an instance is a valid CSS color by the
  :func:`wclr.is_hex_str`, :func:`wclr.is_rgb_function_str`,
  :func:`wclr.is_hsl_function_str`, and :func:`wclr.is_keyword_str` functions.
* The alpha compositing can be performed by the :func:`wclr.alpha_composite`
  function.
* The contrast ratio can be calculated by the :func:`wclr.contrast_ratio`
  function.
