Glossary
========

.. glossary::

   fraction-like
      Object which can instantiate :class:`fractions.Fraction`.
