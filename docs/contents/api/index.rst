API reference
=============

.. autosummary::
   :toctree: generated
   :recursive:

   wclr

.. toctree::

   glossary
   Index </genindex>
