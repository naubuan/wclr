WClr documentation
==================

| **Latest version**: |release|
| **Release date**: |today|

WClr is a Python package for dealing with web colors.

.. rubric:: User guide

* :doc:`contents/installation`
* :doc:`contents/usage`
* :doc:`contents/api/index`

.. rubric:: External link

* `GitLab repository <https://gitlab.com/naubuan/wclr/>`_

.. rubric:: License

`Apache License, Version 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_

.. toctree::
   :caption: User guide
   :maxdepth: 1
   :hidden:

   contents/installation
   contents/usage
   contents/api/index

.. toctree::
   :caption: External link
   :hidden:

   GitLab repository <https://gitlab.com/naubuan/wclr/>
