WClr
====

Python package for dealing with web colors.

Features
--------

* Conversion from/to a value specified in the
  [CSS Color 3](https://www.w3.org/TR/css-color-3/).
* Conversion from/to sRGB and HSL components.
* Alpha compositing.
* Calculation of the contrast ratio.

Requirements
------------

* Python version 3.8 or later

Installation
------------

You can install WClr by, e.g., [pip](https://pip.pypa.io/):

```shell
python -m pip install wclr
```

Usage
-----

See the [documentation](https://naubuan.gitlab.io/wclr/).

License
-------

[Apache License, Version 2.0](LICENSE.txt)
